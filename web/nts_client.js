
/**
 * In this file, the word "tablet" will refer to the simulated tablet,
 * more precisely, to the div #tablet-main.
 * 
 * 
 */

// *** VARIABLES AND SETTINGS INITIALIZATION

var tablet = $("#tablet-main");
var image = $("#tablet-image");
var video = $("#tablet-video");
var webview = $("#tablet-webview-iframe");
webview.attr("src", "about:blank");

// make image not draggable
// because otherwise, when dragging image, 'mouseup' signal is not fired !
image.attr("draggable", false);

var connectionStatusDiv = $("#connection-status");

// tablet width & height are subject to change depending on Pepper model
//  for the first tablet it was 1708*1067
var width = 1280;
var height = 800;

var verbose = true;

var idle_screen_path = "idle_screen.png";
var screen_off_path = "screen_off.png";

var default_host = "127.0.0.1";
var default_port = "31415";

var host = default_host;
var port = default_port;

var wsStates = ['CONNECTING', 'OPEN', 'CLOSING', 'CLOSED'];
// websocket, connected later
var ws = null;

$("#host").val(host);
$("#port").val(port);

$("#connect").on("click", function() {
  console.log("connecting");
  ws.close();
  connectWebsocket();
});

$("#default-connection-settings").on("click", function() {
  $("#host").val(default_host);
  $("#port").val(default_port);
});

$("#select-screen-size").on("change", function() {
  if (verbose) {
    console.log(this.value);
  }
  // screen size parameter was changed
  if (this.value == "1280") {
    width = 1280;
    height = 800;
  } else if (this.value == "1708") {
    width = 1708;
    height = 1067;
  }
  tablet.width(width);
  tablet.height(height);
});

$("#cb-debug").on("click", function() {
  if (this.checked) {
    verbose = true;
  } else {
    verbose = false;
  }
});

// *** WEBSOCKET SETUP

connectWebsocket = function() {
  
  ws = new WebSocket("ws://" + host + ":" + port + "/signal");

  ws.onmessage = function (evt) {
  var data = evt.data;
  if (verbose) {
    console.log("Received message : " + data);
  }
  // here data is supposed to be JSON 

  var obj = jQuery.parseJSON(data);

  method = obj.method;
  args = obj.args;

  if ( !tabletMethods.hasOwnProperty(method) ) {
    throw "No such method : " + method;
  }

  // call the method with its arguments
  methodToCall = tabletMethods[method][0];
  requiredArgs = tabletMethods[method][1];
  checkArgsNumber(args, requiredArgs, methodToCall);

  if (args == null) {
    methodToCall();
  } else {
    methodToCall(...args);
  }

  };

ws.onclose = function() {
  if (verbose) {
    console.log("WebSocket is closed.");
  }
  connectionStatusDiv.html("WebSocket is closed (readyState = " + wsStates[ws.readyState] + ")");
  connectionStatusDiv.removeClass();
  connectionStatusDiv.addClass("red");
};

ws.onopen = function() {
    if(verbose) {
      console.log("connected");
    }
    connectionStatusDiv.html("Connected");
    connectionStatusDiv.removeClass();
    connectionStatusDiv.addClass("green");
 };

};



// *** METHODS CALLED BY THE CLIENT

/**
 * Generic method equivalent to a signal sent by the tablet to the service
 * 
 */
function sendSignal(signal, args) {

  var data = JSON.stringify({
    "type":"signal",
    "signal":signal,
    "args":args
  });

  if (verbose) {
    console.log("sending : " + data);
  }
  ws.send(data);
}

// click on the tablet <=> physical tablet touchDown
tablet.on("mousedown", function(event) {
  if (verbose) {
    console.log(event.offsetX + ", " + event.offsetY);
  }
  var x = event.offsetX;
  var y = event.offsetY;

  sendSignal("onTouchDown", [
    x,
    y
  ]);

});

tablet.on("mouseup", function(event) {
  if (verbose) {
    console.log(event.offsetX + ", " + event.offsetY);
  }
  var x = event.offsetX;
  var y = event.offsetY;

  sendSignal("onTouchUp", [
    x,
    y
  ]);

});

function checkArgsNumber(argsArray, requiredNumber, method) {
  if (argsArray == null && requiredNumber == 0) {
    return true;
  } else if (argsArray.length != requiredNumber) {
    throw "Invalid number of arguments for method " + method + "() - the method will not be called";
    return false;
  }
  return true;
}



// *** Equivalent of ALTabletService functions

function hideEverything() {
  image.attr("hidden", true);
  video.attr("hidden", true);
  webview.css("display", "none");
  tablet.css("background-color", "#ffffff");
}

hideEverything();

// Webview 

function cleanWebview() {
  if (verbose) {
    console.log("cleanWebview()");
  }
  hideEverything();
  webview.attr("src", "about:blank");
  showIdleScreenImage();
}

function executeJS() {
  if (verbose) {
    console.log("executeJS()");
  }
  throw 'Not implemented yet';
}

function hideWebview() {
  if (verbose) {
    console.log("hideWebview()");
  }
  hideEverything();
  showIdleScreenImage();
}

function loadUrl(url) {
  if (verbose) {
    console.log("loadUrl(" + url + ")");
  }
  hideEverything();
  //url = "http://" + host + ":" + port + "/webview/" + url;
  webview.attr("src", url);
}

function showWebview() {
  if (verbose) {
    console.log("showWebview()");
  }
  hideEverything();
  webview.css("display", "block");
}

// Image

function setBackgroundColor(color) {
  if (verbose) {
    console.log("setBackgroundColor(" + color + ")");
  }
  hideEverything();
  // parameter color shoud be an hexa color code
  tablet.css("background-color", color);
}

function showImage(path) {
  if (verbose) {
    console.log("showImage(" + path + ")");
  }
  hideEverything();
  // show image on tablet screen
  image.attr("src", path);
  image.removeAttr("hidden");
}

function hideImage() {
  if (verbose) {
    console.log("hideImage()");
  }
  image.attr("hidden", true);
}

// Video

function playVideo(url) {
  if (verbose) {
    console.log("playVideo(" + url + ")");
  }
  hideEverything();
  tablet.prepend(video);
  video.removeAttr("hidden");
  video.attr("src", url);
  video.attr("autoplay", true);
}

function pauseVideo() {
  if (verbose) {
    console.log("pauseVideo()");
  }
  video.trigger("pause");
}

function resumeVideo() {
  if (verbose) {
    console.log("resumeVideo()");
  }
  video.trigger("play");
}

function stopVideo() {
  if (verbose) {
    console.log("stopVideo()");
  }
  video.trigger("pause");
  video.removeAttr("src");
  video.attr("hidden", true);
}


// gather all the tablet methods, along with their number of required arguments
tabletMethods = {
  "cleanWebview": [ cleanWebview, 0 ],
  "hideWebview": [ hideWebview, 0 ],
  "loadUrl": [ loadUrl, 1 ],
  "showWebview": [ showWebview, 0 ],
  "setBackgroundColor": [ setBackgroundColor, 1 ],
  "showImage": [ showImage, 1 ],
  "hideImage": [ hideImage, 0 ],
  "playVideo": [ playVideo, 1 ],
  "pauseVideo": [ pauseVideo, 0 ],
  "resumeVideo": [ resumeVideo, 0 ],
  "stopVideo": [ stopVideo, 0 ],
};





function showIdleScreenImage() {
  showImage(idle_screen_path);
}

function showScreenOffImage() {
  showImage(screen_off_path);
}

// when the page loads

showIdleScreenImage();

connectWebsocket();
