#!/usr/bin/env python

import Queue
import threading

import qi

import time

import tornado.ioloop
import tornado.websocket
import json

import yaml

# https://stackoverflow.com/questions/1843422/get-webpage-contents-with-python
from six.moves import urllib 

config = yaml.safe_load(open("config/config.yml"))
# TODO check if config file is valid
# TODO allow to override config file with script parameters

app = None

queueSW = None
queueWS = None

def debug(msg):
    # print only if verbose
    if config['verbose']:
        print msg

class ALTabletService:
    
    robot_ip = config['robot_ip']

    # the path we need to replace, can be specific to the simulator used
    dir_to_replace = config['path_correction']['dir_to_replace']
    # the path (relative to the page.html file) 
    replace_with = config['path_correction']['replace_with']

    # signals
    onTouchDown = None
    onTouchUp = None

    ## Variables that are normally at the robot tablet level


    # Tablet Modes
    MODE_SLEEP = 0
    MODE_IMAGE = 1
    MODE_VIDEO = 2
    MODE_WEBVIEW = 3

    tabletMode = MODE_SLEEP

    onTouchScaleFactor = 1.0

    webview_url = None


    def __init__(self):
        debug("__init__ in ALTabletService")
        self.onTouchDown = qi.Signal("m", self.onTouchDownSignal_connect)
        self.onTouchUp = qi.Signal("m", self.onTouchUpSignal_connect)

    def signalReceived(self, data):
        if (data["type"] != "signal"):
            debug("Not a signal")
            return
        signal = data["signal"]

        args = data["args"]
        
        self.trigger[signal](self, args)

    def onTouchDownSignal_connect(self, c):
        debug("onTouchDownSignal_connect (" + str(c) + ")")
    
    def onTouchDownSignal_trigger(self, args):
        debug("triggering onTouchDownSignal(" + str(args) + ")")
        self.onTouchDown(args[0], args[1])

    def onTouchUpSignal_connect(self, c):
        debug("onTouchUpSignal_connect(" + str(c) + ")")
    
    def onTouchUpSignal_trigger(self, args):
        debug("triggering onTouchUpSignal(" + str() + ")")
        self.onTouchUp(args[0], args[1])

    # array of trigger method for each signal
    trigger = {
        "onTouchDown":onTouchDownSignal_trigger,
        "onTouchUp":onTouchUpSignal_trigger
    }

    # when using a simulator, the resource files (images) are not at the same location
    # as when using the real robot. So we need to correct this path.
    # This is done with correct_path() and with parameters dir_to_replace and replace_with.

    def correct_path(self, path):
        if config['path_correction']['enable']:
            if (path.find(self.robot_ip) >= 0 and path.find(self.dir_to_replace) >= 0):
                debug("Correcting path : " + path)
                path = path.replace('http://', '')
                path = path.replace('https://', '')
                path = path.replace(self.robot_ip, '')
                path = path.replace(self.dir_to_replace, self.replace_with)
                debug("Corrected path  : " + path)
        return path

    ##  ALTabletService normal methods

    # Webview methods 

    def cleanWebview(self):
        debug("in simulation ALTabletService : cleanWebview()")
        self.webview_url = None
        self.tabletMode = self.MODE_SLEEP
        self.callMethod("cleanWebview")
    
    def hideWebview(self):
        debug("in simulation ALTabletService : hideWebview()")
        self.tabletMode = self.MODE_SLEEP
        self.callMethod("hideWebview")

    def loadApplication(self, name):
        debug("in simulation ALTabletService : loadApplication(" + name + ")")
        host = config["websocket_server"]["host"]
        port = config["websocket_server"]["port"]
        url = "http://" + host + ":" + port + "/app/" + name + "/html/index.html"

        self.loadUrl(url)

    def loadUrl(self, url):
        debug("in simulation ALTabletService : loadUrl(" + url + ")")
        self.webview_url = url
        self.callMethod("loadUrl", [url])

    def showWebview(self, *path):
        # using path=None (to use None as default argument if no argument is passed to showWebview) 
        #   does not work ("RuntimeError : Arguments types did not match for showWebview()" )
        # so instead, we use "arbitrary arguments" then check if a path was passed or not.
        # According to Philippe D. (from Aldebaran) in 2014, "default arguments are not supported anymore"
        #   (see https://community.ald.softbankrobotics.com/ja/node/1245)
        debug("in simulation ALTabletService : showWebview(" + str(path) + ")")
        if path:
            self.loadUrl(path[0])
        self.tabletMode = self.MODE_WEBVIEW
        self.callMethod("showWebview")

    # Video

    def pauseVideo(self):
        debug("in simulation ALTabletService : pauseVideo()")
        self.callMethod("pauseVideo")

    def playVideo(self, url):
        debug("in simulation ALTabletService : playVideo(" + url + ")")
        self.callMethod("playVideo", [url])
    
    def resumeVideo(self):
        debug("in simulation ALTabletService : resumeVideo()")
        self.callMethod("resumeVideo")

    def stopVideo(self):
        debug("in simulation ALTabletService : stopVideo()")
        self.callMethod("stopVideo")
    
    # Image
    
    def hideImage(self):
        debug("in simulation ALTabletService : hideImage()")
        self.callMethod("hideImage")

    def setBackgroundColor(self, color):
        debug("in simulation ALTabletService : setBackgroundColor(" + color + ")")
        self.callMethod("setBackgroundColor", [color])

    def showImage(self, path):
        debug("in simulation ALTabletService : showImage(" + path + ")")
        path = self.correct_path(path)
        self.callMethod("showImage", [path])

    

    # generic method, that will transmit each specific method call to the client
    def callMethod(self, method, args = None):
        global queueSW
        jsonString = json.dumps({"method":method, "args":args})
        queueSW.put(jsonString)

    # ALTabletService methods whith specific behaviour because of the simulation

    def getOnTouchScaleFactor(self):
        """
            From ALTabletService doc : Get the touch scale factor of current view displayed. Default is 1.0 for all views except for the browser view which is 1.34 .
        """
        if (self.tabletMode == self.MODE_WEBVIEW):
            return 1.34
        return self.onTouchScaleFactor

    def setOnTouchWebviewScaleFactor(self, scaleFactor):
        debug("in simulation ALTabletService : setOnTouchWebviewScaleFactor(" + scaleFactor + ")")
        self.onTouchScaleFactor = scaleFactor

    def robotIp(self):
        #there is no robot ip since there is no robot...
        return "127.0.0.1"

class ServiceThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    
    def run(self):
        global app
        debug("Running ServiceThread")
        app = qi.Application()
        try:
            app.start()
        except RuntimeError:
            print ("RuntimeError when launching qi Application for ALTabletService - Is the naoqi instance running and accessible ?")
            global queueSW
            queueSW.put("quit")
            return
        
        session = app.session
        service = ALTabletService()
        session.registerService("ALTabletService", service)
        debug("Starting ALTAbletService in ServiceThread")
        app.run()

        debug("Service thread exiting")

class WebSocketHandler(tornado.websocket.WebSocketHandler):

    # TODO restrict to only one client 

    instance = None

    def open(self):
        debug("previous instance was " + str(WebSocketHandler.instance))
        remote_ip = self.request.remote_ip
        
        if (WebSocketHandler.instance != None):
            # there was already a client connected

            # behavior depends on configuration
            if config['replace_connection']:
                print "Closing previous connection to " + remote_ip 
                WebSocketHandler.instance.close()
            else:
                self.close()
                print "Connection to " + remote_ip + " cancelled because another client is connected"
                return
        
        # remember the instance
        WebSocketHandler.instance = self 
        debug("WebSocket opened (" + remote_ip + ")")

    @classmethod
    def send_message(self, msg):
        debug("in WebSocketHandler.send_message(" + msg + ")")
        if self.instance:
            self.instance.write_message(msg)

    def on_message(self, message):
        debug("Received : " + message)

        # data is supposed to be JSON here
        #TODO maybe add specific value in payload to check data integrity ?
        try:
            data = json.loads(message)
            msgType = data['type']
            if (msgType == "signal"):
                self.signalReceived(data['signal'], data['args'])
                global queueWS
                queueWS.put(data)
            elif (msgType == "info"):
                self.infoReceived(data["info"], data['args'])
            else:
                print("Error : received message of unknown type : " + msgType)
                return 1
        except ValueError:
            print("Error : Not valid JSON format")
        else:
            pass

    def signalReceived(self, signal, args):
        debug("signalReceived " + signal + str(args))

    def infoReceived(self, info, args):
        debug("infoReceived " + info + args)

    def on_close(self):
        debug("WebSocket closed (" + self.request.remote_ip + ")")

class WebviewRequestHandler(tornado.web.RequestHandler):
    def initialize(self):
        pass
    
    def get(self, resource):
        debug("in WebviewRequestHandler.get(" + resource + ")")
        #TODO case when resource is file:// ?
        webpage = urllib.request.urlopen(resource)
        
        # result depending on status code
        # TODO also treat case when status = 3xx (redirect)
        if (webpage.getcode() == 200):
            # write back the page to the client, that's what we want
            self.write(webpage.read())
        else:
            # return the received status code
            self.set_status(webpage.getcode())
        
        self.flush()
        self.finish()

class AppRequestHandler(tornado.web.RequestHandler):
    def initialize(self, type):
        self.type = type
    
    def get(self, resource):
        debug("in AppRequestHandler.get(" + resource + ")")
        
        if (self.type == "app"):
            # here we serve the application
            app_dir = config["applications"]["directory"]
            page = open(app_dir + "/" + resource)
        elif (self.type == "libs"):
            # and here we serve the libs
            lib_path = config["applications"]["lib"]
            page = open(lib_path)
        
        self.write(page.read())
        self.flush()
        self.finish()



class WebsocketThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        debug("Running WebsocketThread")

        websocket_app = tornado.web.Application([
                            #TODO maybe pass signal and params as arguments ? (simpler)
                            (r"/signal", WebSocketHandler),
                            (r"/webview/(.*)", WebviewRequestHandler),
                            (r"/app/(.*)", AppRequestHandler, dict(type="app")),
                            (r"/libs/(.*)", AppRequestHandler, dict(type="libs")),
                        ])
        port = config['websocket_server']['port']
        websocket_app.listen(port)
        debug("Websocket server is starting on port " + str(port))
        tornado.ioloop.IOLoop.current().start()

        debug("Websocket thread exiting")
    


class MainThread(threading.Thread):

    global queueSW
    global queueWS
    queueSW = Queue.Queue()
    queueWS = Queue.Queue()

    webSocketWorker = WebsocketThread()
    webSocketWorker.start()

    serviceWorker = ServiceThread()
    serviceWorker.start()

    start_time = time.time()

    # TODO implement stop feature
    while time.time() - start_time < 30:

        try:
            # non-blocking get
            msg = queueWS.get(False)
            if isinstance(msg, str) and msg == "quit":
                break
            
            debug("Main thread : received " + str(msg) + " by queueWS")
            app.session.service("ALTabletService").signalReceived(msg)
        
        except Queue.Empty:
            # nothing is received
            pass
        
        try:
            # non-blocking get
            msg = queueSW.get(False)
            if isinstance(msg, str) and msg == "quit":
                break
            
            debug("Main thread : received " + str(msg) + " by queueSW")
            WebSocketHandler.send_message(msg)
        
        except Queue.Empty:
            # nothing is received
            pass
        

    debug("Main thread exiting")

    app.stop()
    tornado.ioloop.IOLoop.current().stop()
    webSocketWorker.join()
    serviceWorker.join()

    exit(0)



if __name__ == '__main__':
    MainThread()
